This is a translated, with grammar enhancements, guide from Loo_Loo on the Minecraft forum. Some of this information may not be applicable to ModLoader for NFC.
# [1.7.3] Loo_Loo's ModLoader Modding Tutorials!

These tutorials are about using ModLoader and ONLY ModLoader! It should help all you newbie modders.

## Part 1 - The Setup

First we need to set up ModLoader inside MCP.

You can get MCP from a site on the Internet so search for Minecraft Coder Pack on Google and it should come up.

You will also need to download [ModLoader](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/1272333-risugamis-mods-updated) from Risugami.

Then, you need to copy all the Minecraft `bin` folder and `resources` folder (With a clean, un-modded minecraft.jar) into the "jars" folder in MCP.

Once that is done, go into `jars` and open the `bin` folder. Open `minecraft.jar` with 7-Zip. Once opened, drag and drop the contents of ModLoader into the MCP `minecraft.jar` file.
Now you can run `decompile.bat`. If it says "2 out of 2 hunks failed" or similar, you can safely ignore it.

## Part 2 - Modding

The first thing you will want to do is open up the `ModLoader.java` and `basemod.java` files in the `.\MCP\src\minecraft\net\src` directory. Create a new file called `mod_${MODNAME}.java`.

Begin your file with:
```java
package net.minecraft.src;
import java.util.Random; // This will be used later in the code.

public class mod_MODNAME extends BaseMod {
    public mod_MODNAME() {
```

To create a new block, under `public mod_MODNAME()` enter:

```java
ModLoader.RegisterBlock( BLOCKNAME );
```

To create a name for this block, you'll use:

```java
ModLoader.AddName(BLOCKNAME, "IngameBlockName");
```

Great! We've got our block registered, and named. We want to override where ModLoader loads the texture for this block, so we'll tell it not to:

```java
IngameBlockName.blockIndexInTexture = ModLoader.addOverride ("/terrain.png", "/resources/PNGFILE.png");
```

Now we've got our own texture on the block. Next is telling ModLoader what version the mod is for:

```java
public String Version() {
    return "1.7.3";
}
```

Under the `public String Version()` method, tell Minecraft that the block exists:

```java
public static final Block IngameBlockName
```

Next up, we set the block's properties.

```java
static {
    blockname = (new Block(99, 0, Material.rock)).setHardness(0.3F).setStepSound(Block.soundStoneFootstep).setBlockName("blockname");
}
```

All that's left is to re-compile it, and it should work. By now, it should look a little something like this:

```java
package net.minecraft.src;
import java.util.Random;

public class mod_MODNAME extends BaseMod {
    public mod_MODNAME() {
        ModLoader.RegisterBlock( OREBLOCK );
        ModLoader.AddName(OREBLOCK, "Ore Name");
        IngameBlockName.blockIndexInTexture = ModLoader.addOverride ("/terrain.png", "/resourcedir/ore.png");
        
    }
    
    public String Version() {
        return "1.7.3";
    }
    
    public static final Block IngameBlockName;
    static {
        blockname = (new Block(99, 0, Material.rock)).setHardness(0.3F).setStepSound(Block.soundStoneFootstep).setBlockName("blockname");
    }
}
```

## Part 3 - Crafting Recipe Basics

Let's give this block a recipe. Back in `public mod_MODNAME()`, we'll add

```java
ModLoader.AddRecipe(new ItemStack(orePickaxe, 1), new Object[] {
        "XXX", " Y ", " Y ", Character.valueOf('X'), INGOT, Character.valueOf('Y'), Item.stick
});
```

This will add a recipe using the block's ingot (Part 5) and sticks to craft a new item, a pickaxe. This can be applied to any kind of standard 3x3 crafting recipe, with any number of item variables, not just X and Y.

## Part 4 - Adding OreGen


If we only wanted to generate one ore type, under `ModLoader.AddName()` we'll add:

```java
@Override
public void GenerateSurface(World world, Random random, int a, int heightAboveBedrock) {
    for(int i = 0; i < 20; i++) {
        int posX = a + random.nextInt(16);
        int posY = random.nextInt(128);
        int posZ = b + random.nextInt(16);
        (new WorldGenMineable(ore.blockID, veinSize)).generate(world, random, posX, posY, posZ);
    }
}
```

To add multiple oretypes, you can do the following:

```java
public void GenerateSurface(World world, Random random, int a, int heightAboveBedrock) {
    for(int i = 0; i < 20; i++){
        int posX=a+random.nextInt(16);
        int posY=random.nextInt(128);
        int posZ=heightAboveBedrock+random.nextInt(16);
        (new WorldGenMinable(oreA.blockID,16)).generate(world,random,posX,posY,posZ);
        for(int j = 0; j < 20; j++){
            posX=a+random.nextInt(16);
            posY=random.nextInt(128);
            posZ=heightAboveBedrock+random.nextInt(16);
            (new WorldGenMinable(oreB.blockID,16)).generate(world,random,posX,posY,posZ);
        }
        for(int j = 0; j < 20; j++){
            posX=a+random.nextInt(16);
            posY=random.nextInt(128);
            posZ=heightAboveBedrock+random.nextInt(16);
            (new WorldGenMinable(oreC.blockID,16)).generate(world,random,posX,posY,posZ);
        }
    }
}
```

## Part 5 - Creating Tools, Items, and Smelting Recipe Basics

We've got our oregen, next up we'll add a smelting recipe before the oregen to turn it into ingots:

```java
ModLoader.AddSmelting(oreBlock.blockID, new ItemStack(oreIngot, 1));
```

The ItemStack method takes two arguments - the item, and the quantity. Now we'll need to name the ingot:

```java
ModLoader.AddName(oreIngot, "Ore Ingot");
```

And finally, register the ingot:

```java
public static final Item oreIngot = new Item(1420).setItemName("oreIngot");
```

That `1420` is the item's ID. This ID must be unique, and cannot override any block ID already present in the game. Next, we'll add the texture override for the item. This is different from the block texture override:

```java
oreIngot.iconIndex = ModLoader.addOverride("/gui/items.png", "/resourcedir/oreIngot.png");
```

Awesome. Ingot's done, smelting's done. Now we'll need to add the tool:

```java
public static final Item orePick = new ItemPickaxe(2005, EnumToolMaterial.WOOD).setItemName("orePick");
```

That EnumToolMaterial has five possible values:

- `WOOD`
- `STONE`
- `IRON`
- `EMERALD` (This isn't 1.3.1 emerald - this is actually diamond!)
- `GOLD` (Put after emerald as it mines faster than it.)

We still need a name for our pickaxe:

```java
ModLoader.AddName(orePick, "Ore Pickaxe");
```

We also need a texture override for it:

```java
orePick.iconIndex = ModLoader.addOverride("/gui/items.png", "/resourcedir/orePick.png");
```

Remember Part 3's crafting tutorial? That's the recipe for this pickaxe!

## Part 6 - Food

I bet you're hungry after all that coding. How about we add in a food item? We first have to allow you to cook the item:

```java
ModLoader.AddSmelting(rawItem.shiftedIndex, new ItemStack(cookedItem, 1));
```

Add your names and texture overrides. Now it's time to make it edible:

```java
public static final Item cookedItem = (new ItemFood(1004, 6, false)).setItemName("cookedItem").setMaxStackSize(1);
```

ItemFood's second argument counts how many half-hearts an item will heal. This food item will heal 3 full hearts.

## Part 7 - Multiplayer

You'll need to set up ModLoader *AND* ModLoaderMP inside of MCP.

